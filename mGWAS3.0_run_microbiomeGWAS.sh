#!/bin/bash
#PBS -N run_microbiomeGWAS
#PBS -l nodes=1:ppn=14
#PBS -l vmem=120GB
#PBS -l walltime=200:00:00
#PBS -j oe
#PBS -M n.deshpande@unsw.edu.au


cd $PBS_O_WORKDIR


module load boost/1.50.0
module load zlib/1.2.8
module load plink/1.9
module load R/3.3.1


# MicrobiomeGWAS:
	# A tool for identifying host genetic variants associated with microbiome composition: https://www.biorxiv.org/content/early/2015/11/10/031187

# Path to tool (This will change for your system)
path_to_tool='microbiomeGWAS/microbiomeGWAS-master'


# Basepath with plink format files (This will change for your system)
base_path_plink_files='plink/plinkv1.9_read_vcf_directly'


# Get binary PED files for microbiomeGWAS input
plink --file $base_path_plink_files/global_output_with_nt14_omit2Samples_in_plink --threads 14 --make-bed


# Get Help 
Rscript microbiomeGWAS-master/R/microbiomeGWAS_v1.0.R -h 


# Sample dataset
#Running the microbiomeGWAS sample data 
Rscript microbiomeGWAS-master/R/microbiomeGWAS_v1.0.R -r $path_to_tool -p microbiomeGWAS-master/data/microbiome.GWAS.Demo.data -d microbiomeGWAS-master/data/distMat379.txt -c microbiomeGWAS-master/data/dataCovariate379.txt -i smoke



# Our dataset with Mothur based distance matrix
Rscript microbiomeGWAS-master/R/microbiomeGWAS_v1.0.R -r $path_to_tool -p plink -d unifrac_mothur_16S_square.dist -c microbiomeGWAS-master/data/dataCovariate379.txt -i smoke





# Making a 'microbiome samples' distance matrix for our dataset 

# This requires a tree file:
#use the  "metaphlan2graphlan.py" script from Metaphyln2

# Use the file 'Metaphyln2_merged_abundance_table_s.txt' from Metaphyln2 datasets
python /share/bioinfo/nandan/scratch02/John_Metagenomics_Nov2014/metaphlan/plotting_scripts/metaphlan2graphlan.py Metaphyln2_merged_abundance_table_s.txt  --tree_file Metaphyln2_merged_abundance_table_s.tree --annot_file Metaphyln2_merged_abundance_table_s.annot
python /share/bioinfo/nandan/scratch02/John_Metagenomics_Nov2014/metaphlan/plotting_scripts/metaphlan2graphlan.py /share/bioinfo/nandan/scratch02/John_Metagenomics_Nov2014/profiled_samples/BM_SRS013506.txt --tree_file sample.tree --annot_file sample.annot

