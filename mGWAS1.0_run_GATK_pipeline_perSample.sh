
#!/bin/bash
#PBS -N create_vcfFile_Host
#PBS -l nodes=1:ppn=15
#PBS -l vmem=120GB
#PBS -l walltime=90:00:00
#PBS -j oe
#PBS -M n.deshpande@unsw.edu.au

cd $PBS_O_WORKDIR

sample_name={SampleID}

date


module load samtools/1.3.1
module load java/jdk1.7.0_07
module load picard/1.115


# Create a vcf file by mapping each sample fastq (Host reads obtained by using DeConSeq) to hg19.fa - human genome reference using GATK : https://software.broadinstitute.org/gatk/

mkdir HostMicrobiome_SNPs

# Multiple steps in GATK pipeline
bwa-0.7.9a/bwa mem -t 15 -R "@RG\tID:{SampleID}\tSM:{SampleID}" /share/bioinfo/nandan/scratch02/tools_db/db/hg19_fasta/hg19.fa {SampleID}_all.fastq.trimmed.single > HostMicrobiome_SNPs/{SampleID}.sam
samtools view -bS HostMicrobiome_SNPs/{SampleID}.sam | samtools sort -o HostMicrobiome_SNPs/{SampleID}.sorted

java -Xmx3g -jar /share/apps/picard/1.115/MarkDuplicates.jar INPUT=HostMicrobiome_SNPs/{SampleID}.sorted OUTPUT=HostMicrobiome_SNPs/{SampleID}.dedup.bam  METRICS_FILE=HostMicrobiome_SNPs/{SampleID}.metricsfile MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=250 ASSUME_SORTED=true VALIDATION_STRINGENCY=SILENT REMOVE_DUPLICATES=True

#Change lable
java -Xmx3g -jar /share/apps/picard/1.115/AddOrReplaceReadGroups.jar I=HostMicrobiome_SNPs/{SampleID}.dedup.bam O=HostMicrobiome_SNPs/{SampleID}.dedup_RG_Final.bam SORT_ORDER=coordinate RGID={SampleID}.dedup_RG_Final RGLB={SampleID} RGPL=illumina RGSM={SampleID} CREATE_INDEX=True RGPU=run_barcode

#Identify regions in need of realignment:
java -Xmx2g -jar /share/bioinfo/nandan/scratch02/tools_db/tools/GATK_3.5/GenomeAnalysisTK.jar -T RealignerTargetCreator -R /share/bioinfo/nandan/scratch02/tools_db/db/hg19_fasta/hg19.fa -o HostMicrobiome_SNPs/{SampleID}_output.intervals  -I HostMicrobiome_SNPs/{SampleID}.dedup_RG_Final.bam

#  Run realigner over intervals
java -Xmx2g -jar /share/bioinfo/nandan/scratch02/tools_db/tools/GATK_3.5/GenomeAnalysisTK.jar -I HostMicrobiome_SNPs/{SampleID}.dedup_RG_Final.bam -R /share/bioinfo/nandan/scratch02/tools_db/db/hg19_fasta/hg19.fa -T IndelRealigner -targetIntervals HostMicrobiome_SNPs/{SampleID}_output.intervals -o HostMicrobiome_SNPs/{SampleID}_realigned.bam

# HaplotypeCaller
java -Xmx2g -jar /share/bioinfo/nandan/scratch02/tools_db/tools/GATK_3.5/GenomeAnalysisTK.jar -T HaplotypeCaller -R /share/bioinfo/nandan/scratch02/tools_db/db/hg19_fasta/hg19.fa -I HostMicrobiome_SNPs/{SampleID}_realigned.bam -stand_call_conf 20.0 -stand_emit_conf 20.0 -o HostMicrobiome_SNPs/raw_snps_{SampleID}_RG_indels_Q20.g.vcf -ERC GVCF



java -Xmx2g -jar /share/bioinfo/nandan/scratch02/tools_db/tools/GATK_3.5/GenomeAnalysisTK.jar -T VariantFiltration -R /share/bioinfo/nandan/scratch02/tools_db/db/hg19_fasta/hg19.fa -V HostMicrobiome_SNPs/raw_snps_{SampleID}_RG_indels_Q20.g.vcf --filterExpression "FS > 30.0 || QD < 2.0" -filterName "STD" -o HostMicrobiome_SNPs/raw_snps_{SampleID}_RG_indels_Q20_VFiltered.g.vcf


rm HostMicrobiome_SNPs/*.sam
rm HostMicrobiome_SNPs/*.dedup.bam
rm HostMicrobiome_SNPs/*.dedup_RG_Final.bam

date
