This readme can be used as a guide to run the scripts developed in the analysis for the manuscript 




(A) Generate shell scripts (per sample) for excecuting the following tools
    ======================================================================
    
		#	Deconseq	: Detect and remove contaminations from your sequence data : http://deconseq.sourceforge.net
		
		#	DymamicTrim/Lengthsort  : SolexaQA 				  	   : http://solexaqa.sourceforge.net/
		
		#	MEGAN6	: An interactive microbiome analysis tool  		   	   : https://ab.inf.uni-tuebingen.de/software/megan6
		
		#	MetaPhlAn2	: Metagenomic Phylogenetic Analysis			   : https://bitbucket.org/biobakery/metaphlan2/src/default/
		
		#	HUMAnN2	: The HMP Unified Metabolic Analysis Network 2		   	   : http://huttenhower.sph.harvard.edu/humann2
		

	python generate_scripts.py 






(B) microbiomeGWAS analysis
    =======================

(1) Get a vcf file for each sample with Host reads mapped to human genome hg19
	
	For each Sample, run the script 
	
	---> qsub mGWAS1.0_run_GATK_pipeline_perSample.sh
	
	Output: raw_snps_{SampleID}_RG_indels_Q20.g.vcf

Output files from Step (1) are input to Step (2)

#########

(2) Run plink 

	---> qsub plink.sh
	
	Output: global_output_with_nt14_omit2Samples_in_plink

#########
Input files to Step (3) 

		global_output_with_nt14_omit2Samples_in_plink (From (2))	
		
		Distance matrix files derived from abundance tables generated in the  MetaPhlAn2 step
	
(3) Run microbiomeGWAS

	---> qsub run_microbiomeGWAS.sh
	



		


