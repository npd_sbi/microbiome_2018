import re
import os

space_split=re.compile("\s+")

def create_generic_script(current_path_RUN1,current_path_RUN2):

	
	# ----- STEP (1) : Retrieve the sample name
	

	list_string='ls -l '+current_path_RUN1+'*/*.fastq >list_RUN1.txt'
	os.system(list_string)	

	list_string='ls -l '+current_path_RUN2+'*/*.fastq >list_RUN2.txt'
	os.system(list_string)	
	

	fileList=open("list_RUN1.txt")
	dataList=fileList.read()
	fileList.close()

	

	# ----- STEP (2) : Use the sample name to create a working directory
	sample_name1=space_split.split(dataList)
	sample_name=sample_name1[8].split('/KAA')[3].split('_L001_R1_001.fastq')[0].split("-")[1].strip()
	path_to_fastq_files=current_path_RUN1+'/KAA'+sample_name1[8].split('/KAA')[3].split('_L00')[0].strip()

	mkdir_string='mkdir analysis/'+sample_name
	os.system(mkdir_string)

	#	Remove the temporary list files
	rm_list1='rm list_RUN1.txt'
	os.system(rm_list1)

	rm_list2='rm list_RUN2.txt'
	os.system(rm_list2)

	
	


	# ----- STEP (3):  Generate the shell script for the specific sample with command for all tools used in the analysis

	file_out_name='analysis/'+sample_name+'/run_pipe_'+sample_name+'.sh'
	file_out=open("%s"%file_out_name,"w")

	out_string=''


	# This is the header section specfic to a particular HPC server. This will change as per the server the user plans to run the scripts on.
	out_string=out_string+'\n#!/bin/bash'
	out_string=out_string+'\n#PBS -N '+sample_name
	out_string=out_string+'\n#PBS -l nodes=1:ppn=5'
	out_string=out_string+'\n#PBS -l vmem=40GB'
	out_string=out_string+'\n#PBS -l walltime=90:00:00'
	out_string=out_string+'\n#PBS -j oe'
	out_string=out_string+'\n#PBS -M n.deshpande@unsw.edu.au'

	out_string=out_string+'\n\ncd $PBS_O_WORKDIR'

	out_string=out_string+'\n\nsample_name='+sample_name
	out_string=out_string+'\n\ndate'

	
	# ----- STEP (4):   Concat
	# CONCAT all files per sample into one
	out_string=out_string+'\n\ncat '+data_path+sample_name+'_*R1* > '+sample_name+'_R1.fastq'
	out_string=out_string+'\n\ncat '+data_path+sample_name+'_*R2* > '+sample_name+'_R2.fastq'
	out_string=out_string+'\ncat '+sample_name+'_R1.fastq '+sample_name+'_R2.fastq > '+sample_name+'.fastq'
	out_string=out_string+'\n\nmv '+data_path+sample_name+'*.fastq ./'
	
	# ----- STEP (5): DymamicTrim/Lengthsort  : SolexaQA
	out_string=out_string+'\n\n### DynamicTrim'
	out_string=out_string+'\nperl DynamicTrim.pl '+sample_name+'_all.fastq'

	out_string=out_string+'\n\n### LengthSort'
	out_string=out_string+'\nperl LengthSort.pl -l 50 '+sample_name+'_all.fastq.trimmed'


	

	# ---- STEP (6) Deconseq : Detect and remove contaminations from your sequence data
	out_string=out_string+'\n\n### DeConSeq'
	out_string=out_string+'\n/share/procbio/nandan/Metagenomics_Nadeem_2016/Metagenomics_2016/RUN_1_October2016/DeconSeq/deconseq-standalone-0.4.3/deconseq.pl -f '+sample_name+'_all.fastq.trimmed.single -id '+sample_name+'_deConDeq'
	out_string=out_string+'\n\ndate'

	
	
	# ---- STEP (7) Metaphlan2 

	out_string=out_string+'\n\n### Metaphyln'
	out_string=out_string+'\n\n/home/z3323934/tools_db/tools/biobakery-metaphlan2-40d1bf693089/metaphlan2.py '+sample_name+'_deConDeq_clean.fq --bowtie2db /home/z3323934/tools_db/tools/biobakery-metaphlan2-40d1bf693089/db_v20/mpa_v20_m200 --nproc 6 --bt2_ps sensitive --input_type multifastq --bowtie2out '+sample_name+'.bt2out > '+sample_name+'_Metaphyln2.txt' 
	#Create a biom file using --input_type bowtie2out H105_S95.bt2out
	out_string=out_string+'\n\n/home/z3323934/tools_db/tools/biobakery-metaphlan2-40d1bf693089/metaphlan2.py --input_type bowtie2out '+sample_name+'.bt2out --biom '+sample_name+'_biom'
	


	# ---- STEP (8)  HUMAnN2

	
	out_string=out_string+'\n\ndate'
	out_string=out_string+'\n\n#HUMAnN2'
	out_string=out_string+'\n\n### HUMAnN2- KEGG'

	# ---> KEGG

	out_string=out_string+'\nmodule load python/2.7.12-bz2'
        out_string=out_string+'\nmodule load diamond/0.8.36'
        out_string=out_string+'\nmodule load bowtie/2.2.9'
        out_string=out_string+'\nmodule load metaphlan2/20170403'

	KEGG_Metaphylan2_base_path='/share/bioinfo/nandan/scratch02/Metagenomics_Nadeem/Metagenomics_2017/combined_analysis_April2017_after_lengthSort/lengthSort_l50/Metaphyln2/'


	out_string=out_string+'\nhumann2 --input '+sample_name+'_deConDeq_clean.fq --output HUMAnN2_out_KEGG --id-mapping '+KEGG_Metaphylan2_base_path+'legacy_kegg_idmapping.tsv --pathways-database '+KEGG_Metaphylan2_base_path+'humann-0.99/data/keggc --protein-database '+KEGG_Metaphylan2_base_path+'custom_database --bypass-nucleotide-search'        

	out_string=out_string+'\n\nhumann2 --input HUMAnN2_out_KEGG/'+sample_name+'_deConDeq_clean_genefamilies.tsv --output HUMAnN2_out_KEGG/MODULES_OUTPUT_DIR --pathways-database /share/bioinfo/nandan/scratch02/Metagenomics_Nadeem/Metagenomics_2017/HUMAnN2/KEGG_legacy_db/full_run/humann-0.99/data/modulec'
	out_string=out_string+'\n\nhumann2_rename_table --input HUMAnN2_out_KEGG/'+sample_name+'_deConDeq_clean_genefamilies.tsv --output HUMAnN2_out_KEGG/'+sample_name+'_deConDeq_clean_genefamilies-names.tsv --names kegg-orthology'
	out_string=out_string+'\nhumann2_rename_table --input HUMAnN2_out_KEGG/'+sample_name+'_deConDeq_clean_pathabundance.tsv --output HUMAnN2_out_KEGG/'+sample_name+'_deConDeq_clean_pathabundance-names.tsv --names kegg-pathway'
	out_string=out_string+'\nhumann2_rename_table --input HUMAnN2_out_KEGG/'+sample_name+'_deConDeq_clean_pathcoverage.tsv --output HUMAnN2_out_KEGG/'+sample_name+'_deConDeq_clean_pathcoverage-names.tsv --names kegg-pathway'



	out_string=out_string+'\n\ndate'


	out_string=out_string+'\n\n### HUMAnN2- Metacyc'

	# ---> Metacyc  

	out_string=out_string+'\n\nhumann2 --input '+sample_name+'_deConDeq_clean.fq --output HUMAnN2_out_Metacyc --nucleotide-database /share/bio/DB/chocophlan/ --protein-database /share/bio/DB/uniref/'
	out_string=out_string+'\nhumann2_rename_table --input HUMAnN2_out_Metacyc/'+sample_name+'_deConDeq_clean_genefamilies.tsv --output HUMAnN2_out_Metacyc/'+sample_name+'_deConDeq_clean_genefamilies-names.tsv --names uniref90'
	out_string=out_string+'\nhumann2_renorm_table --input HUMAnN2_out_Metacyc/'+sample_name+'_deConDeq_clean_genefamilies-names.tsv --output HUMAnN2_out_Metacyc/'+sample_name+'_deConDeq_clean_genefamilies-names-cpm.tsv --units cpm --update-snames'

	#3.3 Regrouping genes to other functional categories
	#HUMAnN2's default "units" of microbial function are gene families and metabolic pathways. However, starting from gene family abundance, it is 	possible to reconstruct the abundance of other functional categories in a microbiome using the humann2_regroup_table script.

	#Regroup our CPM-normalized gene family abundance values to :

	#enzyme commission (EC) categories
	out_string=out_string+'\n\nhumann2_regroup_table --input HUMAnN2_out_Metacyc/'+sample_name+'_deConDeq_clean_genefamilies-names-cpm.tsv --output HUMAnN2_out_Metacyc/level4ec-cpm.tsv --groups uniref90_level4ec'
	#uniref90_ko
	out_string=out_string+'\nhumann2_regroup_table --input HUMAnN2_out_Metacyc/'+sample_name+'_deConDeq_clean_genefamilies-names-cpm.tsv --output HUMAnN2_out_Metacyc/uniref90_ko --groups uniref90_ko'
	#uniref90_go
	out_string=out_string+'\nhumann2_regroup_table --input HUMAnN2_out_Metacyc/'+sample_name+'_deConDeq_clean_genefamilies-names-cpm.tsv --output HUMAnN2_out_Metacyc/uniref90_go --groups uniref90_go'
	#uniref90_pfam
	out_string=out_string+'\nhumann2_regroup_table --input HUMAnN2_out_Metacyc/'+sample_name+'_deConDeq_clean_genefamilies-names-cpm.tsv --output HUMAnN2_out_Metacyc/uniref90_pfam --groups uniref90_pfam'
	#uniref90_infogo1000
	out_string=out_string+'\nhumann2_regroup_table --input HUMAnN2_out_Metacyc/'+sample_name+'_deConDeq_clean_genefamilies-names-cpm.tsv --output HUMAnN2_out_Metacyc/uniref90_infogo1000 --groups uniref90_infogo1000'
	#uniref90_rxn
	out_string=out_string+'\nhumann2_regroup_table --input HUMAnN2_out_Metacyc/'+sample_name+'_deConDeq_clean_genefamilies-names-cpm.tsv --output HUMAnN2_out_Metacyc/uniref90_rxn --groups uniref90_rxn'
	#uniref90_eggnog
	out_string=out_string+'\nhumann2_regroup_table --input HUMAnN2_out_Metacyc/'+sample_name+'_deConDeq_clean_genefamilies-names-cpm.tsv --output HUMAnN2_out_Metacyc/uniref90_eggnog.tsv --groups uniref90_eggnog'

        
	#DELETE BIG FILES
	out_string=out_string+'\n\n#DELETE BIG FILES'	
	out_string=out_string+'\nrm HUMAnN2_out_Metacyc/'+sample_name+'_deConDeq_clean_humann2_temp/*.sam'
	out_string=out_string+'\nrm HUMAnN2_out_Metacyc/'+sample_name+'_deConDeq_clean_humann2_temp/*.bt2'
	out_string=out_string+'\nrm HUMAnN2_out_Metacyc/'+sample_name+'_deConDeq_clean_humann2_temp/*_aligned*'
	out_string=out_string+'\nrm HUMAnN2_out_Metacyc/'+sample_name+'_deConDeq_clean_humann2_temp/*_unaligned*'
	out_string=out_string+'\nrm HUMAnN2_out_Metacyc/'+sample_name+'_deConDeq_clean_humann2_temp/*_custom_chocophlan_database.ffn'
	out_string=out_string+'\nrm HUMAnN2_out_Metacyc/'+sample_name+'_deConDeq_clean_humann2_temp/*_deConDeq_clean_metaphlan_bowtie2.txt'

	out_string=out_string+'\n\nrm HUMAnN2_out_KEGG/MET12_deConDeq_clean_humann2_temp/*_aligned*'
	out_string=out_string+'\nrm HUMAnN2_out_KEGG/MET12_deConDeq_clean_humann2_temp/*_aligned*'

	out_string=out_string+'\nrm *_all.fastq.trimmed'
	out_string=out_string+'\nrm *_all.fastq.trimmed.discard'
	out_string=out_string+'\nrm *_R1.fastq'
	out_string=out_string+'\nrm *_R2.fastq'

	
	

	out_string=out_string+'\n\n#HUMAnN2 END:' 
	out_string=out_string+'\ndate'



	# ---- STEP (9)   MEGAN6 
	out_string=out_string+'\n\n### MEGAN6'

	out_string=out_string+'\n\n#MEGAN6 START:'

	out_string=out_string+'\nmodule load blast+/2.2.28'

	out_string=out_string+'\n\nmkdir diamond_temp'
	out_string=out_string+'\n/home/z3323934/tools_db/tools/diamond_aligner/diamond blastx -d /share/bioinfo/nandan/scratch02/tools_db/db/nr_2016/diamond_nr_index/nr -q '+deconSeq_lengthSorted_files_base_path+sample_name+'_deConDeq_clean.fq.single -f 100 -o '+sample_name+'_deConDeq_clean_MEGAN6.daa -t diamond_temp'

	out_string=out_string+'\n\n#MEGAN6 END:'
	out_string=out_string+'\ndate'


	base_path_MEGAN_tool='/share/bioinfo/nandan/scratch02/tools_db/tools/megan/MEGAM6.6.5/tools/'
	out_string=out_string+'\n\n/share/bioinfo/nandan/scratch02/tools_db/tools/megan/MEGAM6.6.5/tools/daa-meganizer --in '+sample_name+'_deConDeq_clean_MEGAN6.daa --gi2taxa '+base_path_MEGAN_tool+'prot-gi2tax-August2016X.bin -fun EGGNOG INTERPRO2GO SEED --gi2eggnog '+base_path_MEGAN_tool+'gi2eggnog-June2016X.bin --gi2interpro2go '+base_path_MEGAN_tool+'gi2interpro-June2016.bin --gi2seed '+base_path_MEGAN_tool+'gi2seed-May2015X.bin'

	# A shell script for a specific sample
	file_out.write("%s\n"%out_string)
	file_out.close()

	

	return


		


def main():


	# Base path where the raw fastq files for individual samples are placed
	base_path_RUN1='/share/bioinfo/nandan/scratch02/Metagenomics_Nadeem/Metagenomics_2017/full_RUN1/data/KAA3207_20170221-35500465/'
	base_path_RUN2='/share/bioinfo/nandan/scratch02/Metagenomics_Nadeem/Metagenomics_2017/RUN2/data/KAA3207_20170309_run2-35825799/'

	


	# The name(s) of the individual samples in a list. Here 'KAA3207A01-43253390' is a sample name from this work.
	samples_array=['KAA3207A01-43253390']

	
	# Iterate over each sample
	for eachSample in samples_array:
		
		
		# Actual path to the raw fastq files
		current_path_RUN1=base_path_RUN1+eachSample.split("-")[0].strip()
		current_path_RUN2=base_path_RUN2+eachSample.split("-")[0].strip()



		# Create a shell script which runs the following programs per samples
		#	Deconseq	: Detect and remove contaminations from your sequence data : http://deconseq.sourceforge.net
		#	DymamicTrim/Lengthsort  : SolexaQA 				 		   : http://solexaqa.sourceforge.net/
		#	MEGAN6	: An interactive microbiome analysis tool  		   : https://ab.inf.uni-tuebingen.de/software/megan6
		#	MetaPhlAn2	: Metagenomic Phylogenetic Analysis			   : https://bitbucket.org/biobakery/metaphlan2/src/default/
		#	HUMAnN2	: The HMP Unified Metabolic Analysis Network 2		   : http://huttenhower.sph.harvard.edu/humann2
		
	
		create_generic_script(current_path_RUN1,current_path_RUN2)


	return

main()
		


	
